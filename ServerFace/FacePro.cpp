#include "stdafx.h"
#include "FacePro.h"

#include "helper.h"

using namespace cv;
using namespace std;

FacePro::FacePro(){
	unknownPerson = personalData (-1, "unknown", "none", "");
	noClass = personalData (-1, "classroom not trained", "none", "");
	//trainModel(); /*if not used yet, delete*/
	loadExistingModel();
}

void FacePro::loadExistingModel(){
	DIR *dp; struct dirent *ep;     
	dp = opendir ("facemodel");
	if (dp != NULL){
		while (ep = readdir (dp)){
			// check whether ep is the directory "." or a parent ".."
			if (strcmp (ep->d_name, ".") != 0 && strcmp (ep->d_name,"..") != 0){
				string d_name = ep->d_name;
				string model_path = "facemodel/" + d_name;
				//load model
				Ptr<cfr::CustomFaceRecognizer> model1 = cfr::createCustomFisherFaceRecognizer();
				model1->load(model_path);
				//get integer N from the file name "class_N.yml"
				string N_yml = d_name.substr(6, string::npos);
				int int_len = (int) N_yml.size() - 4;
				string N_str = N_yml.substr(0, int_len);
				//add model to map
				cout << "Successfully loading classroom with ID " << N_str << " (" << d_name << ")" << endl;
				mdlClassroom[N_str] = model1;
			}
		}
		cout << "Server has loaded " << mdlClassroom.size() << " classroom's facemodels." << endl;
		(void) closedir (dp);
	}
	
}

Mat FacePro::NormalizeImage(Mat image){
	Mat norm = aisl.normalize(image);
	return norm;
}

Object FacePro::DetectFace(Mat image, double xp, double yp, string save_path){
	CascadeClassifier haar_cascade;
	haar_cascade.load("hc_fuzzygen.xml");

	//absolute
	int xa = (int) xp * image.cols / 100;
	int ya = (int) yp * image.rows / 100;

	// Convert to gray (in case)
	Mat gray;
	cvtColor(image, gray, CV_BGR2GRAY);
	// Normalize gandalf the gray
	Mat norm = NormalizeImage(gray);
	
	// Do haar cascade original gray image
	vector< Rect_<int> > faces_vector;
	vector<Object> faces_jsonArray;
	//Detect face
	haar_cascade.detectMultiScale(norm, faces_vector, 1.2);

	float nearest_distance = (float) image.rows * image.rows + image.cols * image.cols;
	int index_nd = -1;
	for(int i = 0; i < faces_vector.size(); i++) {
		// Process face by face:
		Rect face_r = faces_vector[i];

		//from many candidates check the position of tag
		//the nearest is the face we want
		float rcx = (face_r.br().x + face_r.tl().x ) / (float) 2;
		float rcy = (face_r.br().y + face_r.tl().y ) / (float) 2;
		float distance = (rcx - xa) * (rcx - xa) + (rcy - ya) * (rcy - ya);
		if (distance < nearest_distance){
			nearest_distance = distance;
			index_nd = i;
		}

		//save face region into object
		Object addr_obj;
		addr_obj.push_back(Pair("brx", 100 * face_r.br().x / image.cols));
		addr_obj.push_back(Pair("bry", 100 * face_r.br().y / image.rows));
		addr_obj.push_back(Pair("tlx", 100 * face_r.tl().x / image.cols));
		addr_obj.push_back(Pair("tly", 100 * face_r.tl().y / image.rows));
		faces_jsonArray.push_back(addr_obj);
		if (xp < 0 && yp < 0){
			string face_path = save_path + to_string(i) + ".jpg";
			cout << "   saving ~" << face_path.substr(15) << endl;
			imwrite(face_path, image(face_r));
		}
	}

	if (index_nd != -1 && (xp >= 0 && yp >= 0)){
		//Get the closest face to x,y tag
		Rect face_tag_r = faces_vector[index_nd];
		// Crop the face from the image.
		Mat face_tag_m = image(face_tag_r);
		//save image
		cout << "   saving ~" << save_path.substr(15) << endl;
		imwrite(save_path, face_tag_m);
	}
	
	const Value face_cand( faces_jsonArray.begin(), faces_jsonArray.end() );
	
	Object return_js;
	return_js.push_back(Pair("face_candidates", face_cand));
	return_js.push_back(Pair("face_selection", index_nd));
	
	return return_js;
}


void FacePro::trainNewClass(string class_id, vector<Mat> images, vector<int> labels){
	//create fisherFace Recognizer
	Ptr<cfr::CustomFaceRecognizer> crModel = cfr::createCustomFisherFaceRecognizer();
	crModel->set("threshold", 1377);
	crModel->train(images, labels);
	string fm_save = "facemodel/class_"+class_id+".yml";
	crModel->save(fm_save);

	//add to array holder
	mdlClassroom[class_id] = crModel;
}

vector<Mat> FacePro::prepareImage(vector<Mat> images){
	vector<Mat> newArray;
	for (Mat img : images){
		Mat normIm(128, 128, img.type());
		resize(img, normIm, normIm.size(), 0, 0);
		Mat gray;
		cvtColor(normIm, gray, CV_BGR2GRAY);
		normIm = NormalizeImage(gray);
		newArray.push_back(normIm);
	}
	return newArray;
}

int FacePro::number_ofClassroom(){
	return (int) mdlClassroom.size();
}


void FacePro::openData(string filename, char separator){
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
		exit(1);
	}
	string line, index, name, affiliaton;

	int label = 0;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, index, separator);
		getline(liness, name, separator);
		getline(liness, affiliaton, separator);

		if(!index.empty() && !name.empty()) {
			personalData student(label, name, affiliaton, "");
			infos.push_back(student);
			label++;
		}
	}
}


String FacePro::RecognizeFace(Mat face_query, bool &letsSend, string class_id = "0", string seqDevice_id = ""){
	clock_t tStart = clock();
	// Make image gray
	Mat face_gray, face_resized;
	cvtColor(face_query, face_gray, CV_BGR2GRAY);	
	// Resize face
	cv::resize(face_gray, face_resized, Size(128, 128), 1.0, 1.0, INTER_CUBIC);
	// Normalize
	Mat face_norm = NormalizeImage(face_resized);
	processedImage = face_norm;

	//Log txt
	ofstream lgtime; lgtime.open("temp/" + seqDevice_id + "/recotime.txt", ios::app);
	
	// student
	personalData st = unknownPerson;

	vector<int> topThree ;//= {-1,-1,-1};
	cout << "";
	vector<personalData> topSt;//= {unknownPerson,unknownPerson,unknownPerson};

	int prediction = -1;
	double confidence = 0;
	// Check existence of clasroom id
	if (mdlClassroom.count(class_id)){
		
		//  Check deviceSequence holder for this seqDevice_id
		if (!dist_seqDevice.count(seqDevice_id)){
			deviceSequence nds(seqDevice_id);
			dist_seqDevice.insert(std::pair<string, deviceSequence>(seqDevice_id, nds));
			latestSendResult.insert(std::pair<string, string>(seqDevice_id, ""));
		}
		deviceSequence& curDevSeq = dist_seqDevice.at(seqDevice_id);

		// get distances from the query to all of subspace images of this classroom
		map<int, vector<double>> all_distances;
		mdlClassroom[class_id]->allDistances(face_norm, all_distances);
		
		// put the distance and get prediction until current query
		int status = curDevSeq.addSet(all_distances);
		//curDevSeq.getPrediction(prediction, confidence);
		curDevSeq.getPredictionTop3(topThree, confidence);

		if (status && topThree.size() > 0 && curDevSeq.getVote(topThree[0]) > 30){
			//use prediction to build studentData
			//st = personalData (prediction, "<inquirying info to web>", "", "");
			topSt.push_back( personalData (topThree[0], "<inquirying info to web>", "", ""));
			topSt.push_back( personalData (topThree[1], "<inquirying info to web>", "", ""));
			topSt.push_back( personalData (topThree[2], "<inquirying info to web>", "", ""));
			prediction = topThree[0];
			stringstream sendSummary;
			sendSummary << topThree[0] << "|" << topThree[1] << "|" << topThree[2];
			string sendSummaryString = sendSummary.str();
			if (sendSummaryString != latestSendResult.at(seqDevice_id)){
				latestSendResult.at(seqDevice_id) = sendSummaryString;
			}else{
				letsSend = false;
			}

			cout << "PRED=" << sendSummaryString;
		}else{
			cout << "UNKNOWN ";
			topSt.push_back(unknownPerson);
			letsSend = false;
		}
		
		// create json
		String jsonString = writeToJson(seqDevice_id, topSt, confidence);

		//logging
		curDevSeq.logSumDistance(seqDevice_id);

		// Return
		return jsonString;
	}else{
		cerr << "No such classroom in training set." << endl;
		letsSend = false;
	}
	
	// Return
	return "{}";
}


String FacePro::writeToJson(string seqDevice_id, personalData st, double confidence){
	//Face array
	Object jsonObj = st.toJsonObject();
	char sconfidence[10];
	cout << "444 ";
	sprintf_s(sconfidence, 10, "%.2f", confidence);
	jsonObj.push_back(Pair("confidence", String(sconfidence)));
	jsonObj.push_back(Pair("seqid", seqDevice_id));
	
	std::vector<Object> faces_jsonArray;
	faces_jsonArray.push_back(jsonObj);
		
	const Value val( faces_jsonArray.begin(), faces_jsonArray.end() );
	String jsonString =  write(val);
	return jsonString;
}

String FacePro::writeToJson(string seqDevice_id, vector<personalData> topThreeStd, double confidence){
	//Face array
	std::vector<Object> faces_jsonArray;

	for (personalData pp : topThreeStd){
		Object jsonObj = pp.toJsonObject();
		char sconfidence[10];
		sprintf_s(sconfidence, 10, "%.2f", confidence);
		jsonObj.push_back(Pair("confidence", String(sconfidence)));
		jsonObj.push_back(Pair("seqid", seqDevice_id));
		faces_jsonArray.push_back(jsonObj);
	}
		
	const Value val( faces_jsonArray.begin(), faces_jsonArray.end() );
	String jsonString =  write(val);
	return jsonString;
}


personalData FacePro::getUserInfo_fromWebpage(string class_id, int user_id){
	stringstream urlBuilder;
	urlBuilder << "http://albadr-pc/web/student/info/get_json/" << class_id << "/" << user_id << "?pwd=";
	string url = urlBuilder.str();
	//cout << "querying " << url << endl;
	string json = getContent_fromUrl(url);

	// Parse json
	json_spirit::mValue value;
	json_spirit::read(json, value);
	mObject jsonObject = value.get_obj();
	if (jsonObject.find("info") != jsonObject.end()){
		mObject info = jsonObject["info"].get_obj();

		// Compose personalData
		try{
			string name = info["display_name"].get_str();
			string attendance = info["attendance"].get_str();
			string comment = info["comment"].get_str();

			// Return
			personalData p(user_id, name, attendance, comment);
			return p;
		}
		catch(Exception e){
			return unknownPerson;
		}
	}
	//personalData p(user_id, "ARU", "100", " Love you");
	return unknownPerson;
}
