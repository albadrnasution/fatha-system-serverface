
#ifndef HELPER_H
#define HELPER_H


#include "stdafx.h"
#include "BimaIlumi.h"
#include <sys/stat.h>


// Function which is used by many files
using namespace cv;
using namespace std;


static std::map<string, string> read_setting(){
	std::ifstream file("setting.txt", ifstream::in);
	std::map<string, string> setting;

	string line;
	while (getline(file, line)) {
		stringstream liness(line);

		// settings has format key=value for every line
		string key;
		string value;
		// until the character '=', get key
		getline(liness, key, '=');
		// after that read value untuk new line
		getline(liness, value);
		// put into settings
		setting[key] = value;
	}
	return setting;
}

static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, path, separator);
		//cout << path << endl;
		getline(liness, classlabel);
		if(!path.empty() && !classlabel.empty()) {
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}

static void read_csv_str(const string& filename, vector<string>& imagesPath, vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, path, separator);
		//cout << path << endl;
		getline(liness, classlabel);
		if(!path.empty() && !classlabel.empty()) {
			imagesPath.push_back(path);
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}

/**
*  This function assumse that all images within filename is all subject folders.
*  And then, the images of each subject is seperated in each subject folders.
*  The labels of subject is only integer from zero into subject_folder.size()-1
*  which is the position of the subject folder within imagesPath
*/
static void read_folder_str(const string& filename, vector<string>& images, vector<int>& labels) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	int intLabel = 0;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								//add to container
								images.push_back(imageFullpath);
								labels.push_back(intLabel);
							}
						}
						closedir (dirPerson);
					}

					//next label
					intLabel++;
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

/**
*  This function assumse that all images within filename is all subject folders.
*  And then, the images of each subject is seperated in each subject folders.
*	The labels of subject is only integer from zero into subject_folder.size()-1
*  which is the position of the subject folder within imagesPath
*/
static void read_folder_imagesPerPerson(const string& filename, vector<vector<Mat>>& images) {
	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;
	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;

					//prepare image container for this person
					vector<Mat> personImages;

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								personImages.push_back(imread(imageFullpath, 0));
							}
						}
						closedir (dirPerson);
					}

					//add into images container
					images.push_back(personImages);
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

static void batchResizeImages(const string& filename, bool normalize) {
	std::array<float,25> YaleB_FuzzyRule={10,2,57,142,145,1,0,7,137,118,0,0,2,97,148,0,0,0,57,145,20,2,14,0,31};
	//Illumnation Normalization
	BimaIlumi aisl(YaleB_FuzzyRule);

	//Open directory source and shuffle observations on each person 
	DIR *dir;
	struct dirent *ent;

	string targetRoot = filename + "64";
	if (normalize) targetRoot += "_normalized";
	_mkdir(targetRoot.c_str());

	//open main folder containing folder of subjects
	if ((dir = opendir (filename.c_str())) != NULL) {
		/* read all directory*/
		while ((ent = readdir (dir)) != NULL) {
			switch (ent->d_type) {
			case DT_DIR:
				//if this is a directory and is not a parent
				if (strcmp (ent->d_name, ".") != 0 && strcmp (ent->d_name,"..") != 0){
					//open this directory [this person]
					DIR *dirPerson;
					struct dirent *entPerson;
					string personFolder = filename + "/" + ent->d_name;

					//create target person folder
					string personTargetFolder = targetRoot + "/" + ent->d_name;
					_mkdir(personTargetFolder.c_str());

					//open the directory
					if ((dirPerson = opendir (personFolder.c_str())) != NULL){
						//open the images within this person's directory 
						while ((entPerson = readdir (dirPerson)) != NULL) {
							//again if this is not a parent (!directory checking is not done)
							if (strcmp (entPerson->d_name, ".") != 0 && strcmp (entPerson->d_name,"..") != 0){
								string imageFullpath = personFolder + "/" + entPerson->d_name;
								string imageTargetFullpath = personTargetFolder + "/" + entPerson->d_name;

								Mat img = imread(imageFullpath, CV_LOAD_IMAGE_GRAYSCALE);

								//Do Illumination-Normalization
								if (normalize) img = aisl.normalize(img);

								//Resize normalized image to 64x64
								Mat img64 = Mat(64,64,CV_8U);
								resize(img, img64, Size(64, 64));

								imwrite(imageTargetFullpath.c_str(), img64);
							}
						}
						closedir (dirPerson);
					}
				}
				break;
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("EXIT_FAILURE");
		//return EXIT_FAILURE;
	}
}

// Get the size of a file
static long getFileSize(FILE *file)
{
	long lCurPos, lEndPos;
	lCurPos = ftell(file);
	fseek(file, 0, 2);
	lEndPos = ftell(file);
	fseek(file, lCurPos, 0);
	return lEndPos;
}

static void openFileAsByte(const char* filePath, BYTE** fileBuf, long &len){
	//const char *filePath = "doa.jpg";	
	FILE *file = NULL;		// File pointer

	// Open the file in binary mode using the "rb" format string
	// This also checks if the file exists and/or can be opened for reading correctly
	if (fopen_s(&file, filePath, "rb") == NULL)
		cout << "Could not open specified file" << endl;
	else
		cout << "File opened successfully" << endl;

	// Get the size of the file in bytes
	len = getFileSize(file);

	// Allocate space in the buffer for the whole file
	*fileBuf = new BYTE[len];

	// Read the file in to the buffer
	fread(*fileBuf, len, 1, file);
}

static void printByteAsHex(BYTE* fileBuf, long len){
	cout << "hashedChars: ";
	for (int i = 0; i < len; i++) {
		int iii =  fileBuf[i] < 0 ? 256 + fileBuf[i] % 256 : fileBuf[i];
		cout << setw(2) << setfill('0') << hex << iii << " ";
	}
	cout << endl;
}



/** Accept list of students IDs and open a bunch of face image of that students.
  * This function will create array of Mat of student's faces, the index of array is student's ID.
  * This ID refer to students ID in the WebPage database and for recognition. 
  * The ID starts from 1, so no student with ID Zero.
  *
  * The assumption about location of students face images: 
  *    each student has two folder in fb_photos/face and fu_photos/face in the server,
  *    each folder is managed by WebPage such that the folder contain only images file for training,
  *    all image within folder will be used for training.
  */
static void readImageofStudents_commandFromWeb(string web_path, vector<string> studentIDs, vector<pair<string, vector<Mat>>>& images){

	// Open for all student
	for (string user_id : studentIDs){
		string repo_path = web_path + "uploads/default/";
		string fb_face_path = repo_path + "fb_photos/face/" + user_id + "/";
		//fb: C:\xampp\htdocs\proface2\uploads\default\fb_photos\face\1\*.jpg, name: <imageFbID>.jpg
		string fu_face_path = repo_path + "fu_photos/face/" + user_id + "/";
		//fu: C:\xampp\htdocs\proface2\uploads\default\fu_photos\face\1\*.jpg, name: <imageID_faceIndex>.jpg

		// Prepare an array for this person image
		vector<Mat> personImages;
			
		cout << "  Processing fb_photos, uid=" << user_id << "  ";
		DIR *dp; struct dirent *ep;     
		dp = opendir (fb_face_path.c_str());
		if (dp != NULL){
			int nofimage = 0;
			while (ep = readdir (dp)){
				// check whether ep is the directory "." or a parent ".."
				if (strcmp (ep->d_name, ".") != 0 && strcmp (ep->d_name,"..") != 0){
					//puts (ep->d_name);
					string impath = fb_face_path + ep->d_name;
					personImages.push_back(imread(impath));
					nofimage++;
				}
			}
			cout << "  (loaded "<< nofimage << " images)" << endl;
			(void) closedir (dp);
		}
		else
			perror ("   Couldn't open the directory");
			
		cout << "  Processing fu_photos, uid=" << user_id << "  ";
		dp = opendir (fu_face_path.c_str());
		if (dp != NULL){
			int nofimage = 0;
			while (ep = readdir (dp)){
				// check whether ep is the directory "." or a parent ".."
				if (strcmp (ep->d_name, ".") != 0 && strcmp (ep->d_name,"..") != 0){
					//puts (ep->d_name);
					string impath = fu_face_path + ep->d_name;
					personImages.push_back(imread(impath));
					nofimage++;
				}
			}
			cout << "  (loaded "<< nofimage << " images)" << endl;
			(void) closedir (dp);
		}
		else
			perror ("   Couldn't open the directory");
		
		images.push_back(make_pair(user_id, personImages));
	}
}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
static const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    localtime_s(&tstruct, &now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

/** Get Web content from a url as a string  */
static std::string getContent_fromUrl(string url){
	try
	{
		boost::asio::io_service io_service;

		urdl::read_stream stream(io_service);
		stream.open(url);

		std::stringstream ss;
		for (;;)
		{
			char data[1024];
			boost::system::error_code ec;
			std::size_t length = stream.read_some(boost::asio::buffer(data), ec);
			if (ec == boost::asio::error::eof)
				break;
			if (ec)
				throw boost::system::system_error(ec);
			ss << data;
		}
		return ss.str();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		return "{}";
	}
}

inline bool file_exist_test(const std::string& name){
	struct stat buffer;
	return (stat(name.c_str(), &buffer)==0);
}


#endif // !HELPER_H
