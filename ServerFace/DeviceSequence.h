#ifndef DEVICESEQ_H
#define DEVICESEQ_H

#include "string"
#include "vector"
#include "map"
#include "time.h"
#include "multi_prediction.h"

class deviceSequence {
	static const int DTHRESHOLD_FIRST  = 1600;
	static const int DTHRESHOLD_SECOND = 2000;

private:
	std::string id;
	int total_image;
	long long last_update;
	long long created_at;

	// number of vote of each eprson
	std::map<int, int> sum_vote;
	// distance of last query
	std::map<int, vector<double>> last_query;
	int last_pred;
	double last_conf;

	multi_prediction mp;

public:
	deviceSequence(){
		id = "";
		total_image = 0;
		created_at = (long long) time(nullptr);
		last_pred = -1;
		last_conf = -1;
	};
	deviceSequence(std::string devSeqID){
		id = devSeqID;
		total_image = 0;
		created_at = (long long) time(nullptr);
		last_pred = -1;
		last_conf = -1;
	};

	/**
	* @TODO some other threshold based on confidence??
	* Return true if the distance is used for summation and 
	* false if it is discarded
	*/
	int addSet(std::map<int, vector<double>> all_distance){
		// sort distance to get first and second nearest distance
		multimap<double, int> sorted_dist = mp.order_dist(all_distance);
		map<double, int>::iterator it = sorted_dist.begin();
		double firDist = it->first;it++;
		double secDist = it->first;
		//for logging only
		last_query = all_distance;

		if (firDist < DTHRESHOLD_FIRST){
			mp.addImageQuery(all_distance);

			// 
			map<int, int> vts = mp.getLastVote();
			for (map<int, int>::iterator it = vts.begin(); it != vts.end(); ++it){
				int stdid = it->first, cvote = it->second;
				if (total_image == 0)
					sum_vote.insert(pair<int, int>(stdid, cvote));
				else
					sum_vote.at(stdid) += cvote;
			}

			// Next and logging
			total_image++;
			last_update = (long long) time(nullptr);
			return true;
		} else {
			cout << "DISCARDED ";
			return false;
		}
	}

	void addStidException (int stid){
		mp.addStidException(stid);
		//ignored_stid.insert(stid);
	}

	void reset(){
		mp.resetQuery();	
		total_image = 0;
		created_at = 0;
		last_update = (long long) time(nullptr);
	}

	/**
	* @TODO Confidence
	*/
	void getPrediction(int &prediction, double &confidence){
		if (total_image != 0){
			prediction = mp.predict();
			confidence = 0.0;
			map<int, double> sums = mp.getSummation();

			//for logging
			last_pred = prediction;
			last_conf = confidence;
		}else{
			prediction = -1;
		}
	}
	/**
	* @TODO Confidence
	*/
	void getPredictionTop3(vector<int> &prediction, double &confidence){
		if (total_image != 0){
			prediction = mp.predictThree();
			confidence = 0.0;

			//for logging
			map<int, double> sums = mp.getSummation();
			last_pred = prediction.at(0);
			last_conf = confidence;
		}else{
		}
	}

	void logSumDistance(String seqDevice_id){
		ofstream log; log.open("temp/" + seqDevice_id + "/"+ seqDevice_id +".txt", ios::app);
		// minimum distances of the last image query 
		for (map<int, vector<double>>::iterator it = last_query.begin(); it != last_query.end(); it++){
			vector<double> cp_vect_dist = it->second;
			std::sort(cp_vect_dist.begin(), cp_vect_dist.end());
			log << left <<  it->first << ":" << fixed << setprecision(2) << setw(9) << setfill(' ') << cp_vect_dist.at(0) << " ";
		}
		log  << "    N_IMG =  ";
		map<int, int> vts = mp.getLastVote();
		for (map<int, int>::iterator it = vts.begin(); it != vts.end(); ++it){
			log << left <<  it->first << ":" << fixed <<  setprecision(0) << setw(3) << setfill(' ') << it->second << " ";
		}
		log  << "    P_SUM =  ";
		// summation
		map<int, double> sums = mp.getSummation();
		for (map<int, double>::iterator it = sums.begin(); it != sums.end(); ++it){
			log << left <<  it->first << ":" << fixed <<  setprecision(2) << setw(11) << setfill(' ') << it->second << " ";
		}
		log << "    | PRED = "  << last_pred << ",  CONF = " << last_conf  << endl;
		log.close();
	}

	std::string getId(){
		return id;
	}

	int getTotalImage(){
		return total_image;
	}

	int getVote(int studentID){
		if (total_image == 0 || studentID < 0)
			return 0;
		return sum_vote.at(studentID);
	}
};



#endif
