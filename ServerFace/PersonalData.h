
#include "stdafx.h"

using namespace std;
using namespace json_spirit;

class personalData {

public:
	personalData(){
		index = -1;
		name = "unknown";
		attendance = "none";
		comment = "";
	};
	personalData(int idx, string nm, string att, string cmt){
		index = idx;
		name = nm;
		attendance = att;
		comment = cmt;
	};


	int index;
	string name;
	string attendance;
	string comment;

	Object toJsonObject(){
		Object addr_obj;
		addr_obj.push_back(Pair("index", index));
		addr_obj.push_back(Pair("name",  name));
		addr_obj.push_back(Pair("attendance",  attendance));
		addr_obj.push_back(Pair("comment",  comment));
		return addr_obj;
	}
};

