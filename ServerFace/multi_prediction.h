

#include "facerec_custom.h"
#include <numeric>
#include <functional>


class multi_prediction{

	//iterator type
	typedef int studentid;
	typedef map<studentid, vector<double>>::iterator it_mapv;

	//
	map<studentid, double> summation_perPerson;
	map<studentid, double> last_point;
	map<studentid, int   > last_knn_vote;

	// ignored student id of this sequenc
	std::set<int> ignored_stid;

	//
	int _nquery;
	//int _npeople;

	// current mode
	int mode;

public:
	static const int MODE_MIN_DIST	= 0;
	static const int MODE_INV_DIST	= 1;
	static const int MODE_Q1		= 2;
	static const int MODE_Q2		= 3;
	static const int MODE_MEAN		= 4;
	static const int MODE_INV_ONLY	= 5;
	static const int MODE_RANK_ONLY	= 6;
	static const int MODE_NEG		= 7;
	static const int MODE_INV_ONLY_NN= 8;
	static const int MODE_INV_DIST_NN= 9;
	static const int MODE_INV_DIST_1= 10;
	static const int MODE_KNN_ONE	= 11;
	static const int MODE_VOTE		= 12;
	static const int MODE_KRAW_DIST	= 13;
	static const int N_MODE		= 14;

	multi_prediction(){
		//cusmo = createCustomFisherFaceRecognizer();
		_nquery = 0;
		mode = MODE_INV_ONLY;
	}
	~multi_prediction() {}

	static String modeName(int mode){
		switch (mode){
		case 0:
			return "Min distance   "; 
		case 1:
			return "Inv-rank Reward"; 
		case 2:
			return "Quartile 1     "; 
		case 3:
			return "Q2 (Median)    ";
		case 4:
			return "Mean           "; 
		case 5:
			return "Inv-dist Reward"; 
		case 6:
			return "Rank as Reward "; 
		case 7:
			return "D - d as Reward"; 
		case 8:
			return "Inv-rank k=5   "; 
		case 9:
			return "Inv-rank k=15   "; 
		case 10:
			return "Inv-rank k=1   "; 
		case 11:
			return "kNN k=1   "; 
		case 12:
			return "Simple Voting   "; 
		case 13:
			return "Raw Distance  "; 
		}
		return "";
	}
	// return comparation of the mode
	// true if comparation is greater which mean, the biggest point is the prediction
	// false if smallest is better
	static bool modeCompare(int mode){
		switch (mode){
		case 0: //"Min distance"
			return false; 
		case 1: //"Inv-rank Reward"
			return true;
		case 2: //"Quartile 1"
			return false;
		case 3: //"Quartile 2 (Median)"
			return false;
		case 4: //"Mean"
			return false; 
		case 5: //"Inverse as Reward"
			return true; 
		case 6: //"Rank as Reward"
			return true; 
		case 7: //"Const - d as Reward"
			return true; 
		case 8: //"Inv-rank Reward k=5"
			return true; 
		case 9: //"Inv-rank Reward k=10"
			return true; 
		case 10: //"Inv-rank Reward k=1"
			return true; 
		case 11: //"kNN k=1 (sama kayak mindist)"
			return true;
		case 12: //"Simple Voting"
			return true; 
		case 13: //"Raw Distance"
			return true;

		}
		return false;
	}

	void initSum(map<studentid, vector<double>> all_dist){
		for (it_mapv iter = all_dist.begin(); iter != all_dist.end(); ++iter){
			studentid label = iter->first;
			summation_perPerson.insert(pair<studentid, double>(label, 0.0));
			last_knn_vote.insert(pair<studentid, int>(label, 0));
		}
	}

	void addImageQuery(map<studentid, vector<double>> all_dist){
		// init sums
		if (_nquery == 0){
			initSum(all_dist);
		}
		// reset logging
		for (map<studentid, int>::iterator iter = last_knn_vote.begin(); iter != last_knn_vote.end(); ++iter){
			iter->second = 0;
		}

		map<studentid, double> extracted_score;
		if (mode == MODE_MIN_DIST){
			extracted_score = add_min_distance(all_dist);
		}
		else if (mode == MODE_INV_DIST) {
			extracted_score = add_inv_distance(all_dist);
		}
		else if (mode == MODE_Q1) {
			extracted_score = add_q1(all_dist);
		}
		else if (mode == MODE_Q2) {
			extracted_score = add_median(all_dist);
		}
		else if (mode == MODE_MEAN){
			extracted_score = add_mean(all_dist);
		}
		else if (mode == MODE_INV_ONLY){
			extracted_score = add_inv_only(all_dist, 20);
		}
		else if (mode == MODE_RANK_ONLY){
			extracted_score = add_rank_only(all_dist);
		}
		else if (mode == MODE_NEG){
			extracted_score = add_neg_dist(all_dist);
		}
		else if (mode == MODE_INV_ONLY_NN) {
			extracted_score = add_inv_only(all_dist, 20);
		}
		else if (mode == MODE_INV_DIST_NN) {
			extracted_score = add_inv_distance(all_dist, 15);
		}
		else if (mode == MODE_INV_DIST_1) {
			extracted_score = add_inv_distance(all_dist, 1);
		}
		else if (mode == MODE_KNN_ONE) {
			extracted_score = add_vote(all_dist, 1);
		}
		else if (mode == MODE_VOTE) {
			extracted_score = add_vote(all_dist, 10);
		}
		else if (mode == MODE_KRAW_DIST) {
			extracted_score = add_kraw_dist(all_dist, 10);
		}

		// add extracted_score to summation_points
		for (it_mapv iter = all_dist.begin(); iter != all_dist.end(); ++iter){
			int cp_label = iter->first;
			if (ignored_stid.count(cp_label)==0)
				summation_perPerson.at(cp_label) += extracted_score.at(cp_label);
		}
		// increase query counter
		_nquery++;
		// logging
		last_point = extracted_score;
	}

	/** 
	*  Collect minimum value from each studentid.
	*/
	map<studentid, double>  add_min_distance(map<studentid, vector<double>> dist){
		// initialize minimum value as large as possible
		map<studentid, double> minClassDistance;
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			minClassDistance.insert(pair<studentid, double>(cp_label, DBL_MAX));
		}

		// iterate through all dist, for each studentid (person), find minimum
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			vector<double> cp_vect_dist = iter->second;
			double cp_min_dist = DBL_MAX;
			for (double d : cp_vect_dist){
				if (d < cp_min_dist) cp_min_dist = d;
			}
			minClassDistance.at(cp_label) = cp_min_dist;
		}
		return minClassDistance;
	}

	map<studentid, double> add_q1(map<studentid, vector<double>> dist){
		map<studentid, double>  q1Distance;
		// iterate through all dist, for each key map (person), find quatile 1
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			vector<double> cp_vect_dist = iter->second;

			//sort the vector
			std::sort(cp_vect_dist.begin(), cp_vect_dist.end());

			//find q1
			int vsize = (int) cp_vect_dist.size();
			int r = vsize % 4;
			int k = vsize / 4;
			double quartile_1 = 0;

			switch (r){
			case 0:
				quartile_1 = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
				break;
			case 1:
				quartile_1 = 0.25 * cp_vect_dist.at(k) + 0.75 * cp_vect_dist.at(k-1);
				break;
			case 2:
				quartile_1 = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
				break;
			case 3:
				quartile_1 = 0.75 * cp_vect_dist.at(k) + 0.25 * cp_vect_dist.at(k-1);
				break;
			}

			q1Distance.insert(pair<studentid, double>(cp_label, quartile_1));
		}
		return q1Distance;
	}

	map<studentid, double> add_median(map<studentid, vector<double>> dist){
		map<studentid, double>  medianDistance;
		// iterate through all dist, for each key map (person), find median of each vector
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			vector<double> cp_vect_dist = iter->second;

			//sort the vector
			std::sort(cp_vect_dist.begin(), cp_vect_dist.end());

			//find median
			int vsize = (int) cp_vect_dist.size();
			int r = vsize % 2;
			int k = vsize / 2;
			double median = 0;

			switch (r){
			case 0:
				median = (cp_vect_dist.at(k) + cp_vect_dist.at(k-1)) / 2;
				break;
			case 1:
				median = cp_vect_dist.at(k);
				break;
			}

			medianDistance.insert(pair<studentid, double>(cp_label, median));
		}
		return medianDistance;
	}

	map<studentid, double> add_mean(map<studentid, vector<double>> dist){
		map<studentid, double>  meanDistance;
		// iterate through all dist, for each key map (person), find minimum
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			vector<double> cp_vect_dist = iter->second;

			double sum = std::accumulate(cp_vect_dist.begin(), cp_vect_dist.end(), 0.0);
			double mean = sum / cp_vect_dist.size();

			meanDistance.insert(pair<studentid, double>(cp_label, mean));
		}
		return meanDistance;
	}

	map<studentid, double> add_inv_distance(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  inv_dist_scores;
		// initialization of scores as zero (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			inv_dist_scores.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		// extract top 10 distance and compute points based on inverse distance and weight (10 - rank + 1)
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			double d = it->first;
			int l = it->second;
			if (ignored_stid.count(l)==0){
				double p = weight * 1000 / d;
				inv_dist_scores.at(l) += p;
				//logging
				last_knn_vote.at(l) += 1;
				// Next
				weight--;
				if (weight == 0) break;
			}
		}

		return inv_dist_scores;
	}

	map<studentid, double> add_neg_dist(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  neg_dist;
		const int DEF_POINT = 5000;
		// initialization of scores as zero (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			neg_dist.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		// extract top 10 distance and compute points based on negative
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			double d = it->first;
			int l = it->second;
			double p = DEF_POINT - d;
			neg_dist.at(l) += p;
			// Next
			weight--;
			if (weight == 0) break;
		}

		return neg_dist;
	}


	map<studentid, double> add_inv_only(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  inv_dist_scores;
		// initialization of scores as zero (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			inv_dist_scores.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		// extract top 10 distance and compute points based on inverse distance
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			double d = it->first;
			int l = it->second;

			if (ignored_stid.count(l)==0){
				// take account only if the label is NOT in ignore list
				double p = 1000 / d;
				inv_dist_scores.at(l) += p;
				// Next
				weight--;
				if (weight == 0) break;
			}
		}

		return inv_dist_scores;
	}

	map<studentid, double> add_rank_only(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  rank_scores;
		// initialization of scores as zero (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			rank_scores.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		// extract top 10 distance and compute points based on rank only
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			int l = it->second;
			// Rank is deriving weight
			rank_scores.at(l) += weight;
			// Next
			weight--;
			if (weight == 0) break;
		}

		return rank_scores;
	}

	map<studentid, double> add_vote(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  votes;
		// initialization of scores as zero vote (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			votes.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		// extract top 10 distance 
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			int l = it->second;
			// VOTE = 1
			votes.at(l) += 1;
			weight--;
			if (weight == 0) break;
		}
		return votes;
	}

	map<studentid, double> add_kraw_dist(map<studentid, vector<double>> dist, int krank = 10){
		map<studentid, double>  sumraw;
		// initialization of scores as zero vote (student has no knn)
		for (it_mapv iter = dist.begin(); iter != dist.end(); ++iter){
			int cp_label = iter->first;
			sumraw.insert(pair<studentid, double>(cp_label, 0));
		}

		// container for distance ASC ordered
		multimap<double, int> dist_ordered = order_dist(dist);

		int weight = krank;
		double least_raw_dist = DBL_MAX;
		// extract top 10 distance
		for (multimap<double, int>::iterator it = dist_ordered.begin(); it != dist_ordered.end(); ++it){
			int l = it->second;
			double raw_d = it->first;
			//Raw Distance as points
			sumraw.at(l) += raw_d;
			weight--;
			if (weight == 0) {
				least_raw_dist = raw_d * 2;
				break;
			}
		}
		// for student without any knn, put penalty
		for (int i=0; i<sumraw.size(); ++i){
			if (sumraw.at(i) == 0) sumraw.at(i) = least_raw_dist;
		}
		return sumraw;
	}

	void resetQuery(){
		for (int i=0; i < summation_perPerson.size(); i++){
			summation_perPerson[i] = 0;
		}
		summation_perPerson.clear();
		_nquery = 0;
	}

	int predict(){
		bool grCompare = modeCompare(mode);
		// decide whether to predict by max score or min score
		if (grCompare){
			// sort biggest first, DESC
			multimap<double, int, std::greater<double>> sorted;
			for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
				int label = it->first;
				double distn = it->second;
				//insert, biggest first
				sorted.insert( std::pair<double, int>(distn, label));
			}
			// the biggest points is in the front of map
			multimap<double, int>::iterator it = sorted.begin();
			// return label with biggest distance
			return it->second;
		}
		else {
			// sort smallest first, ASC
			multimap<double, int> sorted;
			for (map<int, double>::iterator it = summation_perPerson.begin(); it != summation_perPerson.end(); ++it){
				int label = it->first;
				double distn = it->second;
				//insert, smallest first
				sorted.insert( std::pair<double, int>(distn, label));
			}
			// the smallest points is in the front of map
			multimap<double, int>::iterator it = sorted.begin();
			// return label with smallest distance
			return it->second;
		}
	}
	
	void addStidException (int stid){
		ignored_stid.insert(stid);
	}

	vector<int> predictThree(){
		bool grCompare = modeCompare(mode);
		vector<int> predictions;
		map<int, double> tobeSorted = summation_perPerson;
		// decide whether to predict by max score or min score
		if (grCompare){
			//first remove the ignoredIDs
			for (int ign : ignored_stid){
				tobeSorted.erase(ign);
			}

			// sort biggest first, DESC
			multimap<double, int, std::greater<double>> sorted;
			for (map<int, double>::iterator it = tobeSorted.begin(); it != tobeSorted.end(); ++it){
				int label = it->first;
				double distn = it->second;
				//insert, biggest first
				sorted.insert( std::pair<double, int>(distn, label));
			}

			// the biggest points is in the front of map
			multimap<double, int>::iterator it = sorted.begin();
			//first
			int firstPrediction = it->second;
			//second
			it++;
			int secondPrediction = it->second;
			//third
			it++;
			int thirdPrediction = it->second;

			// return the three
			predictions.push_back(firstPrediction);
			predictions.push_back(secondPrediction);
			predictions.push_back(thirdPrediction);
			return predictions;
		}
		else {
			//first remove the ignoredIDs
			for (int ign : ignored_stid){
				tobeSorted.erase(ign);
			}
			// sort smallest first, ASC
			multimap<double, int> sorted;
			for (map<int, double>::iterator it = tobeSorted.begin(); it != tobeSorted.end(); ++it){
				int label = it->first;
				double distn = it->second;
				//insert, smallest first
				sorted.insert( std::pair<double, int>(distn, label));
			}

			// the smallest points is in the front of map
			multimap<double, int>::iterator it = sorted.begin();
			//first
			int firstPrediction = it->second;
			//second
			it++;
			int secondPrediction = it->second;
			//third
			it++;
			int thirdPrediction = it->second;
			// return the three
			predictions.push_back(firstPrediction);
			predictions.push_back(secondPrediction);
			predictions.push_back(thirdPrediction);
			return predictions;
		}

		return predictions;
	}

	
	/**
	 *  Order the distances within vector in ascending order.
	 *  Usually to see which distance is the least and owned by whom.
	 */
	multimap<double, int> order_dist(map<int, vector<double>> dist){
		// container for distance ASC ordered
		multimap<double, int> dist_ordered;

		// iterate through all dist to reorganize it to multimap, small in front first
		for (map<int, vector<double>>::iterator iter = dist.begin(); iter != dist.end(); iter++){
			vector<double> cp_vect_dist = iter->second;
			int cp_label = iter->first;
			for (double d : cp_vect_dist){
				dist_ordered.insert(pair<double, int>(d, cp_label));
			}
		}
		return dist_ordered;
	}


	map<studentid, double> getSummation(){
		return summation_perPerson;
	}

	map<studentid, int> getLastVote(){
		return last_knn_vote;
	}

	map<studentid, double> getLastPoint(){
		return last_point;
	}
};

