#ifndef ServerHandler_H
#define ServerHandler_H

#include "helper.h"
#include "FacePro.h"


class ServerHandler {
public:
	ServerHandler::ServerHandler();

	void on_open(connection_hdl hdl) {
		m_connections.insert(hdl);
	}

	void on_close(connection_hdl hdl) {
		m_connections.erase(hdl);
	}
	
	void on_message(connection_hdl hdl, server::message_ptr msg);

	void run(uint16_t port) {
		m_server.listen(port);
		m_server.start_accept();
		m_server.run();
	}

	string handlerMessage_recognize(connection_hdl hdl, mObject);
	string handlerMessage_train(mObject);
	string handlerMessage_detect(mObject);
private:
	typedef std::set<connection_hdl,std::owner_less<connection_hdl>> con_list;
	server m_server;
	con_list m_connections;
	FacePro fp;
	
	std::map<string, string> setting;
};


#endif // !ServerHandler_H
