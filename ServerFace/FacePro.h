
#ifndef FC_FACEPRO_H
#define FC_FACEPRO_H

#include "stdafx.h"
#include "BimaIlumi.h"
#include "PersonalData.h"
#include "DeviceSequence.h"
#include "facerec_custom.h"

using namespace cv;
using namespace std;
namespace cfr = customfacerec;

class FacePro {
    int im_width, im_height;

	//illumination normalization model
	BimaIlumi aisl;
	//training model
	Ptr<cfr::CustomFaceRecognizer> model;
	//training model of class
	map<string, Ptr<cfr::CustomFaceRecognizer>> mdlClassroom;
	//training images path
	string trainingPath;
	
	//total number of image (int) for every seqDevice (string)
	//can be generalized to device info/state (last_update)
	map<string, deviceSequence> dist_seqDevice;
	map<string, string> latestSendResult;

	///static const int conf_threshold = 5;
	///static const int distance_threshold_1 = 800;
	///static const int distance_threshold_2 = 1200;

	
	String FacePro::writeToJson(string seqDevice_id, personalData st, double confidence);
	String FacePro::writeToJson(string seqDevice_id,  vector<personalData>  st, double confidence);

public:
	FacePro();
	Mat NormalizeImage(Mat);
	Object FacePro::DetectFace(Mat image, double x, double y, string save_path);
	String FacePro::RecognizeFace(Mat, bool&, string, string);
	personalData getUserInfo_fromWebpage(string, int);

	void addStidException(string seqDevice_id, int stid){
		dist_seqDevice.at(seqDevice_id).addStidException(stid);
	}


	//train new model for a classroom
	void loadExistingModel();
	void trainNewClass(string, vector<Mat>, vector<int>);
	int number_ofClassroom();
	vector<Mat> FacePro::prepareImage(vector<Mat>);

	//Saving for person list
	personalData unknownPerson;
	personalData noClass;
	vector<personalData> infos;

	//Logging variables
	Mat processedImage;

private:
	void trainModel();
	void openData(string,char separator = ';');
};

#endif 
