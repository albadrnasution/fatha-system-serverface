#include "stdafx.h"
#include "BimaIlumi.h"

using namespace cv;
using namespace std;

BimaIlumi::BimaIlumi(){
	array<float,25> YaleB_FuzzyRule={10,2,57,142,145,1,0,7,137,118,0,0,2,97,148,0,0,0,57,145,20,2,14,0,31};

	for (int i=0;i<25;i++)
		fuzzyRule[i] = YaleB_FuzzyRule[i];
}

BimaIlumi::BimaIlumi(array<float,25> inputFuzzyRule){
	if (inputFuzzyRule.size() == 25){
		for (int i=0;i<25;i++)
			fuzzyRule[i] = inputFuzzyRule[i];
	}
}

void BimaIlumi::initSourceImage(Mat src){
	height=src.size().height;
	width=src.size().width;
	step=src.step;
	channels=src.channels();
	imgNormalized = Mat(height,width,CV_8U);
	imgSource = src;
}

Mat BimaIlumi::normalize(Mat imgSource){
	initSourceImage(imgSource);

	float **appearance,**label,**color;
	int **datalhe, **filter;

	Mat imgLocalBHE=Mat(height,width,CV_8U);

	dataImgNorm=(uchar *)imgNormalized.data;
	dataImgLBHE=(uchar *)imgLocalBHE.data;

	// Sub-Block Overlap Histogram Equalization
	appearance=new float *[height];
	for(int i=0;i<height;i++)
		appearance[i]=new float [width];

	label=new float *[height];
	for(int i=0;i<height;i++)
		label[i]=new float [width];

	color=new float *[height];
	for(int i=0;i<height;i++)
		color[i]=new float [width];

	filter=new int *[height+1];
	for(int i=0;i<=height;i++)
		filter[i]=new int [width+1];

	datalhe=new int *[height];
	for(int i=0;i<height;i++)
		datalhe[i]=new int [width];

	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			datalhe[i][j]=0;

	// Create spatio-temporal filter
	int I=0;
	for(int i=1;i<=height;i++)
	{
		int J=0;
		for(int j=1;j<=width;j++)
		{
			if(I==0 && J==0 || I==0 && J==(width/halfSize)-1 || I==(height/halfSize)-1 && J==0 || I==(height/halfSize)-1 && J==(width/halfSize)-1)
				filter[i-1][j-1]=1;
			else if(I==0 && J>=1 && J<=(width/halfSize)-1 || J==0 && I>=1 && I<=(height/halfSize)-1 || I==(height/halfSize)-1 && J>=1 && J<=(width/halfSize)-1 || J==(width/halfSize)-1 && I>=1 && I<=(height/halfSize)-1)
				filter[i-1][j-1]=2;
			else
				filter[i-1][j-1]=4;

			if(j%halfSize==0)
				J++;
		}
		if(i%halfSize==0)
			I++;
	}

	// Subblock Histogram Equalization
	for(int i=0;i<height-halfSize;i+=halfSize)
	{
		for(int j=0;j<width-halfSize;j+=halfSize)
		{
			int x=0;
			for(int k=i;k<i+wndwSize;k++)
			{
				int y=0;
				for(int l=j;l<j+wndwSize;l++)
				{
					subblock.p[x*wndwSize+y] = k >= height || l >= width ? 0 : imgSource.at<uchar>(k, l) % 256;
					y++;
				}
				x++;
			}
			HistEq(wndwSize);

			x=0;
			for(int k=i;k<i+wndwSize;k++)
			{
				int y=0;
				for(int l=j;l<j+wndwSize;l++)
				{
					if (k < height && l < width)
						datalhe[k][l]+=hist.p[x*wndwSize+y];
					y++;
				}
				x++;
			}
		}
	}

	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			dataImgLBHE[i*step+j*channels]=(int)((float)datalhe[i][j]/filter[i][j]);

	//cvSmooth(lhe,lhe,CV_BLUR,5);
	blur(imgLocalBHE,imgLocalBHE,Size(5,5));

	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			int imgVal = imgSource.at<uchar>(i, j) % 256;
			// Appearance Model
			appearance[i][j]=dataImgLBHE[i*step+j*channels];

			if(appearance[i][j]>255)
				appearance[i][j]=255;

			// Shadow Model
			label[i][j]=(float)imgVal/256;

			if(label[i][j]>=1)
				label[i][j]=0.999;

			// Illumination Normalization
			float a=fuzzy_a(label[i][j],appearance[i][j]);
			float b=1.2*label[i][j];

			color[i][j]=((a+1)/(b*a+1))*imgVal;

			if(color[i][j]>255)
				color[i][j]=255;

			if(color[i][j]<0)
				color[i][j]=0;

			dataImgNorm[i*step+j*channels]=color[i][j];
		}
	}

	//Resize(cnt);

	//Save(cnt);
	for(int i=0;i<height;i++)
		delete[] datalhe[i];
	delete[] datalhe;

	for(int i=0;i<height;i++)
		delete[] filter[i];
	delete[] filter;

	for(int i=0;i<height;i++)
		delete[] appearance[i];
	delete[] appearance;

	for(int i=0;i<height;i++)
		delete[] label[i];
	delete[] label;

	for(int i=0;i<height;i++)
		delete[] color[i];
	delete[] color;

	return imgNormalized;
}


void BimaIlumi::HistEq(int size)
{
	float d, temp, *lut, *h;

	lut=new float [260];
	h=new float [260];

	for(int i=0;i<256;i++)
		h[i]=0;

	d=1./(size*size);
	for(int i=0;i<size;i++)
		for(int j=0;j<size;j++)
			h[subblock.p[i*size+j]]+=d;

	temp=0;
	for(int i=0;i<256;i++)
	{
		temp+=h[i];
		lut[i]=temp*256+0.5;  /// 255 make white black
	}

	for(int i=0;i<size;i++)
		for(int j=0;j<size;j++)
			hist.p[i*size+j]=lut[subblock.p[i*size+j]];

	delete[] h;
	delete[] lut;
}


float BimaIlumi::fuzzy_a(float a,float b)
{
	float s_dmf[100],t_dmf[100],rb[300];
	float fuzz,temp,temp1;
	float rule1,rule2;
	int no_rule;

	//float r[25]={10,2,57,142,145,1,0,7,137,118,0,0,2,97,148,0,0,0,57,145,20,2,14,0,31};		// Ok for BHE w/o spatial filter

	no_rule=5;
	rule1=1./(no_rule-1);
	rule2=255./(no_rule-1);

	// Fuzzyfikasi : Degree of MF Segmentation
	int j=0;
	s_dmf[0]=triangle(0,0,rule1,a);
	for(int i=1;i<no_rule-1;i++)
	{
		s_dmf[i]=triangle(j*rule1,(j+1)*rule1,(j+2)*rule1,a);
		j++;
	}
	s_dmf[no_rule-1]=triangle((no_rule-2)*rule1,1,1,a);

	// Fuzzyfikasi : Degree of MF Sigmoid
	j=0;
	t_dmf[0]=triangle(0,0,rule2,b);
	for(int i=1;i<no_rule-1;i++)
	{
		t_dmf[i]=triangle(j*rule2,(j+1)*rule2,(j+2)*rule2,b);
		j++;
	}
	t_dmf[no_rule-1]=triangle((no_rule-2)*rule2,255,255,b);

	// Rule Base
	temp=0;
	for(int i=0;i<no_rule;i++)
	{
		for(int j=0;j<no_rule;j++)
		{
			//rb[i*no_rule+j]=minimum(s_dmf[i],t_dmf[j]);
			rb[i*no_rule+j]=s_dmf[i]*t_dmf[j];
			temp+=rb[i*no_rule+j];
		}
	}

	// Output (Defuzzyfikasi)
	temp1=0;
	for(int i=0;i<no_rule;i++)
		for(int j=0;j<no_rule;j++)
			temp1 += rb[i*no_rule+j] * fuzzyRule[i*no_rule+j];

	fuzz=temp1/temp;

	return(fuzz);
}

float BimaIlumi::triangle(float a,float b,float c,float x)
{
	float d;

	if(x>=a && x<b)
		d=(x-a)/(b-a);
	else if(x>=b && x<c)
		d=(c-x)/(c-b);
	else
		d=0;

	return(d);
}