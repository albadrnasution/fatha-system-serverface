
#include "stdafx.h"
#include "ServerHandler.h"
#include "base64.h"

ServerHandler::ServerHandler() {
	// Set logging settings
	//m_server.get_alog().set_ostream();
	m_server.set_access_channels(websocketpp::log::alevel::all);
	m_server.clear_access_channels(websocketpp::log::alevel::frame_payload | websocketpp::log::alevel::message_header | websocketpp::log::alevel::frame_header);
	m_server.init_asio();

	m_server.set_open_handler(bind(&ServerHandler::on_open,this,::_1));
	m_server.set_close_handler(bind(&ServerHandler::on_close,this,::_1));
	m_server.set_message_handler(bind(&ServerHandler::on_message,this,::_1,::_2));
	// Read setting for this program
	setting = read_setting();
	if (setting.count("COUT_TIME") == 0) setting["COUT_TIME"] = "1";
	std::cout << "=== Setting ===" <<endl;
	std::cout << "Web Path : " << setting["WEBSITE_FCPATH"] << endl;
}

void ServerHandler::on_message(connection_hdl hdl, server::message_ptr msg) {
	int s = (int) msg->get_payload().size();
	//std::cout << "=== Received : " << s << "bytes" << std::endl;
	//std::cout << "[" << s << "bytes]" ;

	//assume msg always json
	const std::string message = msg->get_payload();
	json_spirit::mValue value;
	auto jsonYes = json_spirit::read(message, value);

	if (jsonYes){
		mObject jsonObject = value.get_obj();
		string source = jsonObject["source"].get_str();
		string request = jsonObject["request"].get_str();

		if (source == "device"){
			std::cout << "[DVC]";

			try {
				// get info which is a base64 encoded jpeg image

				if (request == "recognize"){
					std::cout << "[Reco]" ;
					handlerMessage_recognize(hdl,jsonObject);

					//if (setting["COUT_TIME"]=="1") log << "Time Sending Reply: " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

					std::cout << endl;
				}else if (request == "update"){

				}else if (request == "ignore_stid"){
					std::cout << "[Igno]" ;
					int ignored_stid = jsonObject["info"].get_int();
					string faceseqid = jsonObject["faceseqid"].get_str();
					std::cout << " " << faceseqid << " STID:" << ignored_stid << endl ;
					fp.addStidException(faceseqid, ignored_stid);				

				}
			}catch(Exception e){
				cerr << endl << e.msg << endl;
			}
		}
		else if (source == "web"){
			std::cout << "=== Src: " << source << ", Req: " << request << endl;
			std::cout << "****************************************&!*@^!*@&^*$^~" << endl;
		
			mObject info = jsonObject["info"].get_obj();
			string jsonReply = "";

			// Compose and Send a JSON Reply
			try {
				if (request == "detect"){	
					jsonReply = handlerMessage_detect(info);
				}
				else if (request == "train"){
					jsonReply = handlerMessage_train(info);
				}
				// Output for Logging
				std::cout << endl << "JSON Reply: " << endl << jsonReply << endl << endl;
				// Send jsonReply
				m_server.send(hdl, jsonReply, websocketpp::frame::opcode::TEXT);
			} catch (const websocketpp::lib::error_code& e) {
				std::cout << "Echo failed because: " << e << "(" << e.message() << ")" << std::endl;
			} catch (...){
				std::cout << "Unknown Error occurred when processing web request." << endl;
				// Send jsonReply
				m_server.send(hdl, "{'stat':'failed'}", websocketpp::frame::opcode::TEXT);
			}
		} 
	}else{
		std::cout << "GOT Plain Message [" << s << "bytes] " << message << endl;
		m_server.send(hdl, "Hi!", websocketpp::frame::opcode::TEXT);
		std::cout << "Sendin Hi!" << endl;
	}
}
	

string ServerHandler::handlerMessage_recognize(connection_hdl hdl, mObject jsonObject){
	string faceseqid = jsonObject["faceseqid"].get_str();
	string info = jsonObject["info"].get_str();

	clock_t tStart = clock();
	//Log txt
	ofstream log; log.open("temp/" + faceseqid + "/time.txt", ios::app);
	// decode base64 and extract c_str
	string  decoded = base64_decode(info);

	///decode base64 to vector and then directly decode jpg into mat (still error)
	///std::vector<BYTE>  decoded = base64_decode_vector(info);
	///Mat m = imdecode(decoded, CV_LOAD_IMAGE_COLOR);

	// make up a new filename of the retrieved jpeg image with timestamp
	time_t timer; long long now = time(&timer);
	std::stringstream ss; ss << "temp/" << faceseqid ;
	String folder = ss.str();
	_mkdir(folder.c_str());
	ss.str(std::string()); 
	ss << folder << "/f" << now;
	String fileName;
	char fileX = 'A' - 1;
	do{
		fileX++;
		fileName = ss.str()  + fileX + ".jpg";
	}while(file_exist_test(fileName));
	if (setting["COUT_TIME"]=="1") log << "" << folder << "/" << now << fileX-1 << endl;

	// write array of byte of retrieved jpeg image
	// if there are same file received in the same timestamp (and same devSeq), it will be replaced
	std::ofstream outfile(fileName, std::ofstream::binary);
	outfile.write (decoded.c_str(), decoded.length());
	outfile.close();

	// (processing time debugging)
	if (setting["COUT_TIME"]=="1") log << "Time Receive image: " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

	tStart = clock();
	// Open image for further processing
	Mat query = imread(fileName);
	if (setting["COUT_TIME"]=="1") log << "Time Reading image: " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

	tStart = clock();
	// Face processing
	string class_id = jsonObject["class_id"].get_str();
	std::cout << " (CR " << class_id << ", SQ " << faceseqid << ", IM " << now << ") ";
	bool letsSend = true;
	String jsonReply = fp.RecognizeFace(query, letsSend, class_id, faceseqid);
	if (setting["COUT_TIME"]=="1") log << "Time Recognition++: " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

	tStart = clock();
	// If send-worthy, lets send
	if (letsSend){
		//Send reply
		String forSend = jsonReply;
		try {
			//send jsonReply
			m_server.send(hdl, forSend, websocketpp::frame::opcode::TEXT);
			cout << " SENT";
		} catch (const websocketpp::lib::error_code& e) {
			std::cerr << "Echo failed because: " << e << "(" << e.message() << ")" << std::endl;
		} 
	}else{
		cout << " !!";
	}
	if (setting["COUT_TIME"]=="1") log << "Time Sending Image : " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

	// Save processed image to file for clarification
	tStart = clock();
	ss.str(std::string()); ss << "temp/" << faceseqid << "/pr" << now << fileX << ".jpg";
	String prFilename = ss.str();
	imwrite(prFilename, fp.processedImage);
	if (setting["COUT_TIME"]=="1") log << "Time Saving Image : " << 1000 * (clock() - tStart) / CLOCKS_PER_SEC << " ms" << endl;

	return jsonReply;
}

string ServerHandler::handlerMessage_detect(mObject info){
	string slug = info["slug"].get_str();
	string image_id = info["iid"].get_str();
	string user_id = info["uid"].get_str();
	string site_fcpath = setting["WEBSITE_FCPATH"];//info["fcpath"].get_str();
	string repo_path = site_fcpath + info["path"].get_str();
	string jsonReply = "{}";
	Object face_candidate;

	// Check Slug
	if (slug == "fb"){
		std::cout << "Processing FB Image " << repo_path <<  "  " << info["uid"].get_str() << "/" << info["iid"].get_str() << endl;
		
		string uidf_path = repo_path + "face/" + info["uid"].get_str();
		_mkdir(uidf_path.c_str());
		string uid_path = repo_path + "face_exclude/" + info["uid"].get_str();
		_mkdir(uid_path.c_str());
		string img_path = info["uid"].get_str() + "/" + info["iid"].get_str() + ".jpg";
		string savepath = repo_path + "face_exclude/" + img_path;
		string loadpath = repo_path + "original/" + img_path;
		String x = info["x"].get_str();
		String y = info["y"].get_str();
		std::cout << "(x,y) = " << x << ", " << y << endl;

		Mat image = imread(loadpath, CV_LOAD_IMAGE_COLOR); 
		
		std::string::size_type sz;     // alias of size_t
		double xdbl = std::stod (x, &sz);
		double ydbl = std::stod (y, &sz);

		//do face detection
		cout << "Path to save: " << endl << savepath.substr(15) << endl;
		face_candidate = fp.DetectFace(image, xdbl, ydbl, savepath);
	}
	else if (slug == "file_upload"){
		cout << "Processing File Uploaded Image" << endl;

		string uid_path = repo_path + "candidate/" + info["uid"].get_str();
		_mkdir(uid_path.c_str());
		string img_path = "/i" + info["iid"].get_str() + "_c";
		string savepath = uid_path + img_path;
		string loadpath = site_fcpath + info["picture"].get_str();
		Mat image = imread(loadpath, CV_LOAD_IMAGE_COLOR);

		cout << "Faces repo path:" << endl << savepath.substr(15) << endl;
		face_candidate = fp.DetectFace(image, -1, -1, savepath);
	}

	// Add ID and Slug of image for returned JSON
	face_candidate.push_back(Pair("img_id", image_id));
	face_candidate.push_back(Pair("slug", slug));
	// Convert object to string
	return write(face_candidate);
}

string ServerHandler::handlerMessage_train(mObject info){
	string class_name = info["class_name"].get_str();
	string class_id = info["class_id"].get_str();
	cout << "Class: " << class_name << " (" << class_id << ")" << endl;
	mArray students = info["student_ids"].get_array();
	vector<string> studentIDs;
	vector<pair<string, vector<Mat>>> imagesPair;

	cout << "Students: ";
	for (mValue s : students){
		cout << s.get_str() << " ";
		string sid = s.get_str();
		studentIDs.push_back(sid);
	}
	cout << endl;

	readImageofStudents_commandFromWeb(setting["WEBSITE_FCPATH"], studentIDs, imagesPair);

	cout << "****************************************&!*@^!*@&^*!^@" << endl;
	cout << "Attempting training for class '" << class_name << "' " << endl;
	// Compose label and image for training (Converting array)
	vector<int> labelsArray; vector<Mat> imagesArray;
	for (pair<string, vector<Mat>> pyupyu : imagesPair){
		cout << "  student " << pyupyu.first << " : " << pyupyu.second.size() << " images" << endl;
		for (Mat pyuMat : pyupyu.second){
			labelsArray.push_back(atoi(pyupyu.first.c_str()));
			imagesArray.push_back(pyuMat);
		}
	}

	// Train!
	imagesArray = fp.prepareImage(imagesArray);
	fp.trainNewClass(class_id, imagesArray, labelsArray);
	cout << "Now we have " << fp.number_ofClassroom() << " classrooms model." << endl;

	// Number?
	int total_student = (int) studentIDs.size();
	int total_images = (int) imagesArray.size();
	string date =  currentDateTime(); 

	//put info success and training data;
	std::stringstream ss;
	ss << "{\"success\":\"yes\", \"training\":{\"status\":\"" << date << "\", ";
	ss << "\"number_of_students\": "<<total_student<<", \"number_of_images\": "<<total_images<< ", ";
	ss << "\"number_of_im_per_st\": [";
	char* separator = "";
	for (pair<string, vector<Mat>> apair : imagesPair){
		ss << separator;
		ss <<  "{\"sid\":" << apair.first << ", \"nim\":" << apair.second.size() << "}";
		separator = ",";
	}
	ss << "]}}";

	return ss.str();
}

